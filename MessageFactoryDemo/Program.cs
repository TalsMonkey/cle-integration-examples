﻿using System;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace MessageFactoryDemo
{
	public static class Program
	{
		private const string ServiceBusConnectionStringQueue = "Endpoint=sb://itis-pp.servicebus.windows.net/;SharedAccessKeyName=example;SharedAccessKey=b2MBIlLPRp3bObknTdfDgdA6wBRPwcktElRuac4glZ8=";
		private const string ServiceBusConnectionStringTopic = "Endpoint=sb://itis-pp.servicebus.windows.net/;SharedAccessKeyName=example;SharedAccessKey=jmT9OPlaSprA+TMxLZgDKDBth0JcMvIJUodGT81zQa8=";

		private const string QueueName = "integration-example";
		private const string TopicName = "integration-example.events";

		private static QueueClient _queueClient;
		private static TopicClient _topicClient;

		private static void Main(string[] args)
		{
			MainAsync().GetAwaiter().GetResult();
		}

		private static async Task MainAsync()
		{
//			const string xml =
//				"<PublishMessage xmlns=\"http://example.cle.com/2018/02/PublishMessage\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"><Data>Message 0</Data></PublishMessage>";
//			
//			var serializer = new DataContractSerializer(typeof(ClePrivateIncomingMessage));
//			using (var stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml))) 
//			{
//				
//				var msg = (ClePrivateIncomingMessage)serializer.ReadObject(stream);
//				stream.Flush();
//			}
//
//			Console.ReadKey();
//			return;
//			
			_queueClient = QueueClient.CreateFromConnectionString(ServiceBusConnectionStringQueue, QueueName);
			_topicClient = TopicClient.CreateFromConnectionString(ServiceBusConnectionStringTopic, TopicName);

			Console.WriteLine($"Receiving messages from queue: {QueueName} and sending to topic: {TopicName}.");
			Console.WriteLine("Press any key to exit after completion.\n");

			// Register the queue message handler and receive messages in a loop
			RegisterOnMessageHandlerAndReceiveMessages();

			Console.ReadKey();

			await _queueClient.CloseAsync();
			await _topicClient.CloseAsync();
		}

		private static void RegisterOnMessageHandlerAndReceiveMessages()
		{
			//Add processing handler
			_queueClient.OnMessage(ProcessMessage, new OnMessageOptions{AutoComplete = false, MaxConcurrentCalls = 1});
		}

		private static async void ProcessMessage(BrokeredMessage message)
		{
			var messageId = message.MessageId;
			var messageLockToken = message.LockToken;
			try
			{
				// Process the message from the queue
				var messageBody = message.GetBody<ClePrivateIncomingMessage>(new DataContractSerializer(typeof(ClePrivateIncomingMessage)));
				Console.WriteLine($"Received message:\t{messageBody.Data}");

				var processedMessage = CreateProcessedMessage(messageBody);
			
				// Send the processed message to the topic
				await SendMessageAsync(processedMessage);

				// Complete the message from the queue so that it is not received again
				await _queueClient.CompleteAsync(messageLockToken);
			}
			catch (Exception exception)
			{
				Console.WriteLine($"Failed to process message {messageId}. {exception.Message}.{Environment.NewLine} {exception.StackTrace}");
				await _queueClient.CompleteAsync(messageLockToken);
			}

		}

		private static ClePrivateOutgoingMessage CreateProcessedMessage(ClePrivateIncomingMessage messageBody)
		{
			var data = new string(messageBody.Data.Reverse().ToArray());
			var newMessageBody = new ClePrivateOutgoingMessage {NewData = data};
			return newMessageBody;
		}

		private static async Task SendMessageAsync(ClePrivateOutgoingMessage processedMessage)
		{
			try
			{
				var message = new BrokeredMessage(processedMessage, new DataContractSerializer(typeof(ClePrivateOutgoingMessage)));
				// Write the body of the message to the console
				Console.WriteLine($"Sending message:\t{processedMessage.NewData}");

				// Send the message to the topic
				await _topicClient.SendAsync(message);
			}
			catch (Exception exception)
			{
				Console.WriteLine($"{DateTime.Now} Exception: {exception.Message}");
			}
		}
	}

	//CLE data contracts. Integrators will have to implement the own compatible
	//implementaions for serialization and sending to ServiceBus
	[DataContract(Name = "PublishMessage", Namespace = "http://example.cle.com/2018/02")]
	internal class ClePrivateIncomingMessage
	{
		[DataMember]
		public string Data { get; set; }
	}

	[DataContract(Name = "ConsumeMessage", Namespace = "http://example.cle.com/2018/02")]
	internal class ClePrivateOutgoingMessage
	{
		[DataMember]
		public string NewData { get; set; }
	}
}
