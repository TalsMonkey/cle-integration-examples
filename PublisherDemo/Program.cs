﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

//using Microsoft.ServiceBus.Messaging;
//using QueueClient = Microsoft.Azure.ServiceBus.QueueClient;

namespace PublisherDemo
{
	public static class Program
	{
		private const string ServiceBusConnectionString =
			"Endpoint=sb://itis-pp.servicebus.windows.net/;SharedAccessKeyName=example;SharedAccessKey=b2MBIlLPRp3bObknTdfDgdA6wBRPwcktElRuac4glZ8=";

		private const string QueueName = "integration-example";

		private static QueueClient _queueClient;

		private static void Main(string[] args)
		{
			MainAsync().GetAwaiter().GetResult();
		}

		private static async Task MainAsync()
		{
			const int numberOfMessages = 10;
			_queueClient = QueueClient.CreateFromConnectionString(ServiceBusConnectionString, QueueName); //new QueueClient(ServiceBusConnectionString, QueueName);

			Console.WriteLine($"Sending {numberOfMessages} messages to queue: {QueueName}.");
			Console.WriteLine("Press any key to exit after completion.\n");

			// Send messages
			await SendMessagesAsync(numberOfMessages);

			Console.ReadKey();

			await _queueClient.CloseAsync();
		}

		private static async Task SendMessagesAsync(int numberOfMessagesToSend)
		{
			try
			{
				for (var i = 0; i < numberOfMessagesToSend; ++i)
				{
					await SendMessageAsync(i);
				}
			}
			catch (Exception exception)
			{
				Console.WriteLine($"{DateTime.Now} Exception: {exception.Message}");
			}
		}

		private static async Task SendMessageAsync(int i)
		{
			//Create message to send
			var publishMessage = new PublishMessage {Data = $"Message {i}"};
			var message = CreateBrokeredMessage(publishMessage);
			message.Properties["Provider-ID"] = "GMT";
			
			// Send the message to the queue
			Console.WriteLine($"Sending message:\t{publishMessage.Data}");
			await _queueClient.SendAsync(message);
		}

		private static BrokeredMessage CreateBrokeredMessage(PublishMessage publishMessage)
		{
			
			return new BrokeredMessage(publishMessage, new DataContractSerializer(typeof(PublishMessage)));
			// Create a new message to send to the queue
//			var body = GetSerializedBody(publishMessage);
//			var message = new Message(body);
//			return message;
		}

		private static byte[] GetSerializedBody(PublishMessage publishMessage)
		{
			var settings = new DataContractSerializerSettings{};
			var serializer = new DataContractSerializer(typeof(PublishMessage), settings);
			using (var stream = new MemoryStream())
			{
				serializer.WriteObject(stream, publishMessage);
				var buffer = stream.GetBuffer();
				var value = System.Text.Encoding.UTF8.GetString(buffer);
				return buffer;
			}
		}
	}

	[DataContract(Namespace = "http://example.cle.com/2018/02")]
	internal class PublishMessage
	{
		[DataMember]
		public string Data { get; set; }
	}
}