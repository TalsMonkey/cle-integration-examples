﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;

namespace ConsumerDemo
{
	public static class Program
	{
		private const string ServiceBusConnectionString = "Endpoint=sb://itis-pp.servicebus.windows.net/;SharedAccessKeyName=example;SharedAccessKey=jmT9OPlaSprA+TMxLZgDKDBth0JcMvIJUodGT81zQa8=";
		private const string TopicName = "integration-example.events";
		private const string SubscriptionName = "mysub";

		private static ISubscriptionClient _subscriptionClient;

		private static void Main(string[] args)
		{
			MainAsync().GetAwaiter().GetResult();
		}

		private static async Task MainAsync()
		{
			_subscriptionClient = new SubscriptionClient(ServiceBusConnectionString, TopicName, SubscriptionName);

			Console.WriteLine($"Receiving messages from topic: {TopicName}.");
			Console.WriteLine("Press any key to exit after completion.\n");

			// Register the queue message handler and receive messages in a loop
			RegisterOnMessageHandlerAndReceiveMessages();

			Console.ReadKey();

			await _subscriptionClient.CloseAsync();
		}

		private static void RegisterOnMessageHandlerAndReceiveMessages()
		{
			// Configure the message handler options
			var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
			{
				MaxConcurrentCalls = 1,
				AutoComplete = false
			};

			// Register the function that processes messages
			_subscriptionClient.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);
		}

		private static async Task ProcessMessagesAsync(Message message, CancellationToken token)
		{
			var serializer = new DataContractSerializer(typeof(ConsumeMessage));
			// Process the message from the topic
			using (var stream = new MemoryStream(message.Body))
			{
				var consumeMessage = (ConsumeMessage)serializer.ReadObject(stream);
				Console.WriteLine($"Received message:\t{consumeMessage.Data}");

				// Complete the message from the queue so that it is not received again
				await _subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
			}
		}

		private static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
		{
			Console.WriteLine($"Message handler encountered an exception: {exceptionReceivedEventArgs.Exception}");

			var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
			Console.WriteLine("Exception context for troubleshooting:");
			Console.WriteLine($"Endpoint: {context.Endpoint}");
			Console.WriteLine($"Entity Path: {context.EntityPath}");
			Console.WriteLine($"Executing Action: {context.Action}");

			return Task.CompletedTask;
		}
	}
	
	//CLE data contracts. Integrators will have to implement the own compatible
	//implementaions for serialization and sending to ServiceBus
	[DataContract(Namespace = "http://example.cle.com/2018/02")]
	internal class ConsumeMessage
	{
		[DataMember(Name="NewData")]
		public string Data { get; set; }
	}
}
